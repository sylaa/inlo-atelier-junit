/**
 * décembre 2015
 *
 * @author Mirko Steinle
 */
public class Account
{
    private String accountNumber;
    private double balance;
    private double creditLimit;

    /**
     * Constructor for account.
     *
     * @param accountNumber  Business identifier of this account, mandatory.
     * @param initialBalance The initial balance of this account, may be negative.
     * @param creditLimit    The credit limit of this account, positive or zero.
     */
    public Account(String accountNumber, double initialBalance, double creditLimit)
    {
        if (accountNumber == null || accountNumber.isEmpty())
        {
            throw new IllegalArgumentException("Argument [accountNumber] is mandatary. Received: " + accountNumber);
        }
        if (creditLimit < 0)
        {
            throw new IllegalArgumentException("Agument [creditLimit] should be zero or positive. Received: " + creditLimit);
        }
        if (initialBalance < -creditLimit)
        {
            throw new IllegalArgumentException("The initial balance must not be less than the credit limit [creditLimit: " + creditLimit + ", intialBalance: " + initialBalance);
        }
        this.accountNumber = accountNumber;
        this.balance = initialBalance;
        this.creditLimit = creditLimit;
    }

    public double getBalance()
    {
        return balance;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    /**
     * Debits the given amount from this account.
     * @param amount the amount to debit, strictly positive.
     */
    public boolean debit1(double amount)
    {
        if (amount <= 0)
        {
            return false;
        }
        if (balance - amount < -creditLimit)
        {
            return false;
        }
        balance -= amount;
        return true;
    }

    /**
     * Credits the given amount to this account.
     *
     * @param amount the amount to credit, strictly positive.
     */
    public boolean credit1(double amount)
    {
        if (amount <= 0)
        {
            return false;
        }
        balance += amount;
        return true;
    }

    /**
     * Credits the given amount to this account.
     *
     * @param amount the amount to credit, strictly positive.
     * @throws IllegalArgumentException if amount is not strictly positive
     */
    public void credit(double amount)
    {
        if (amount <= 0)
        {
            throw new IllegalArgumentException("Argument [amount] should be positive. Received: " + amount);
        }
        balance += amount;
    }

    /**
     * Debits the given amount from this account.
     * @param amount the amount to debit, strictly negative.
     * @throws IllegalArgumentException if amount is not strictly positive
     * @throws InsufficientFundsException if the balance of this account would drop below the credit limit
     */
    public void debit(double amount)
    {
        if (amount <= 0)
        {
            throw new IllegalArgumentException("Argument [amount] should be positive. Received: " + amount);
        }
        if (balance - amount < -creditLimit)
        {
            throw new InsufficientFundsException("debit " + amount, this);
        }
        balance -= amount;
    }

    @Override
    public String toString()
    {
        return "Account{" +
                "accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                ", creditLimit=" + creditLimit +
                '}';
    }
}
