import org.junit.Assert;
import org.junit.Test;


/**
 * décembre 2015
 *
 * Exemples de tests basiques.
 * @author Mirko Steinle
 */
public class AccountTest1
{
    @Test
    public void testCredit1_ok() throws Exception
    {
        // Setup / Mise-en-place
        Account a = new Account("a", 30.5, 0);

        // Execute / Excercice
        boolean result = a.credit1(50);

        // Verify / Vérification
        Assert.assertTrue(result);
        Assert.assertEquals(80.5, a.getBalance(), 0);

        // Tear-down / Désactivation
        // => pas nécessaire dans ce cas
    }

    @Test
    public void testCredit1_amount0() throws Exception
    {
        // Setup
        Account a = new Account("a", 30.5, 0);

        // Execute
        boolean result = a.credit1(0);

        // Verify
        Assert.assertFalse(result);
        Assert.assertEquals("Le solde ne devrait pas changer", 30.5, a.getBalance(), 0);
    }

    @Test
    public void testCredit1_amountNegative() throws Exception
    {
        // Setup
        Account a = new Account("a", 30.5, 0);

        // Execute
        boolean result = a.credit1(-10);

        // Verify
        Assert.assertFalse(result);
        Assert.assertEquals("Le solde ne devrait pas changer", 30.5, a.getBalance(), 0);
    }
}