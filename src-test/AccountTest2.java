import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * décembre 2015
 *
 * Shared setup method.
 * @author Mirko Steinle
 */
public class AccountTest2
{

    Account a;

    @Before
    public void setUp() throws Exception
    {
        a = new Account("a", 30.5, 0);
    }

    @Test
    public void testCredit1_ok() throws Exception
    {
        // Execute
        boolean result = a.credit1(50);

        // Verify
        Assert.assertTrue(result);
        Assert.assertEquals(80.5, a.getBalance(), 0);
    }

    @Test
    public void testCredit1_amount0() throws Exception
    {
        // Execute
        boolean result = a.credit1(0);

        // Verify
        Assert.assertFalse(result);
        Assert.assertEquals(30.5, a.getBalance(), 0);
    }
}